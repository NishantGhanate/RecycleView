package com.example.nishant.recycleviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import java.io.DataInput;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> SubjectNames;
    List<Integer> image;
    RecyclerView recyclerView;

    RecyclerView.LayoutManager RecyclerViewLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.recycleview);

        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(RecyclerViewLayoutManager);
        AddItems(); // DATA SET
        RecyclerView.Adapter adapter = new RecyclerViewAdapter(SubjectNames,image);
        recyclerView.setAdapter(adapter);
    }

    public void AddItems(){
        image = new ArrayList<>();
        SubjectNames = new ArrayList<>();
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);

        SubjectNames.add(" C++");
        SubjectNames.add(" C ");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_item,menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) menuItem.getActionView();

     /*   searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
*/

        return super.onCreateOptionsMenu(menu);

    }
}
