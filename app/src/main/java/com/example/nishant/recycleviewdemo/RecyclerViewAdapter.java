package com.example.nishant.recycleviewdemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nishant on 12/6/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {    //.ViewHolder class
    ArrayList<String> SubjectNames;
    List<Integer>ImageViews;
    View view1;
    public RecyclerViewAdapter(ArrayList<String> subjectNames, List<Integer> image) { // class constructor
        this.SubjectNames = subjectNames;
        this.ImageViews = image;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item,parent,false);
        return new ViewHolder(view1);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        holder.subjectTextView.setText(SubjectNames.get(position));
        holder.imageView.setImageResource(ImageViews.get(position));
    }

    @Override
    public int getItemCount() {
        return SubjectNames.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView subjectTextView;
        ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            subjectTextView = (TextView) itemView.findViewById(R.id.textViewSubject);
            imageView = (ImageView)itemView.findViewById(R.id.image);
        }
    }

}
